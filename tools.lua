-- -- PICKAXE DEFAULT
-- minetest.override_item("default:pick_wood", {
-- 	wield_scale = {x=1.5, y=2, z=1},
-- })

-- minetest.override_item("default:pick_stone", {
-- 	wield_scale = {x=1.5, y=2, z=1},
-- })

-- minetest.override_item("default:pick_bronze", {
-- 	wield_scale = {x=1.5, y=2, z=1},
-- })

-- minetest.override_item("default:pick_steel", {
-- 	wield_scale = {x=1.5, y=2, z=1},
-- })

-- minetest.override_item("default:pick_mese", {
-- 	wield_scale = {x=1.5, y=2, z=1},
-- })

-- minetest.override_item("default:pick_diamond", {
-- 	wield_scale = {x=1.5, y=2, z=1},
-- })

-- -- SWORDS DEFAULT
-- minetest.override_item("default:sword_diamond", {
-- 	wield_scale = {x=1.5, y=2, z=1},
-- })

-- minetest.override_item("default:sword_wood", {
-- 	wield_scale = {x=1.5, y=2, z=1},
-- })

-- minetest.override_item("default:sword_stone", {
-- 	wield_scale = {x=1.5, y=2, z=1},
-- })

-- minetest.override_item("default:sword_steel", {
-- 	wield_scale = {x=1.5, y=2, z=1},
-- })

-- minetest.override_item("default:sword_bronze", {
-- 	wield_scale = {x=1.5, y=2, z=1},
-- })

-- minetest.override_item("default:sword_mese", {
-- 	wield_scale = {x=1.5, y=2, z=1},
-- })

-- -- AXE DEFAULT
-- minetest.override_item("default:axe_diamond", {
-- 	wield_scale = {x=1.5, y=2, z=1},
-- })

-- minetest.override_item("default:axe_wood", {
-- 	wield_scale = {x=1.5, y=2, z=1},
-- })

-- minetest.override_item("default:axe_stone", {
-- 	wield_scale = {x=1.5, y=2, z=1},
-- })

-- minetest.override_item("default:axe_steel", {
-- 	wield_scale = {x=1.5, y=2, z=1},
-- })

-- minetest.override_item("default:axe_bronze", {
-- 	wield_scale = {x=1.5, y=2, z=1},
-- })

-- minetest.override_item("default:axe_mese", {
-- 	wield_scale = {x=1.5, y=2, z=1},
-- })

minetest.override_item("", {
	wield_scale = {x=0.75,y=1.25,z=4},
	wield_image = "x_wieldhand.png"
})

-- -- SHOVEL DEFAULT
-- minetest.override_item("default:shovel_wood", {
-- 	wield_scale = {x=1.5, y=2, z=1},
-- })

-- minetest.override_item("default:shovel_stone", {
-- 	wield_scale = {x=1.5, y=2, z=1},
-- })

-- minetest.override_item("default:shovel_bronze", {
-- 	wield_scale = {x=1.5, y=2, z=1},
-- })

-- minetest.override_item("default:shovel_steel", {
-- 	wield_scale = {x=1.5, y=2, z=1},
-- })

-- minetest.override_item("default:shovel_mese", {
-- 	wield_scale = {x=1.5, y=2, z=1},
-- })

-- minetest.override_item("default:shovel_diamond", {
-- 	wield_scale = {x=1.5, y=2, z=1},
-- })
